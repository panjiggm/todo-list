import React from "react";
import PropTypes from "prop-types";

function TodoItem({ todo, onDelete, onToggle }) {
  const handleConfirm = (todo) => {
    if (window.confirm(`Are you sure want to remove "${todo.title}"?`)) {
      onDelete(todo.id);
    }
  };

  return (
    <div className="card">
      <div className="subcard">
        <h4
          className="card__title"
          style={{ textDecoration: todo.completed ? "line-through" : "none" }}>
          {todo.title}
        </h4>

        <div className="checkbox-wrapper">
          <input
            type="checkbox"
            className="checkbox"
            checked={todo.completed}
            onChange={() => onToggle(todo.id)}
          />
          <label style={{ color: todo.completed ? "#2ECC71" : null }}>
            Completed
          </label>
        </div>
      </div>

      <div className="card__action">
        <button onClick={() => handleConfirm(todo)}>
          <i className="fas fa-trash" />
          <span>Delete</span>
        </button>
      </div>
    </div>
  );
}

TodoItem.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    completed: PropTypes.bool,
  }),
  onDelete: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
};

export default TodoItem;
