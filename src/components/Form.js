import React from "react";
import PropTypes from "prop-types";

function Form({ todos, onHandleChange, onCreate, loading }) {
  const handleSubmit = (e) => {
    e.preventDefault();

    onCreate();
  };

  return (
    <div className="forms">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={todos.title}
          onChange={(e) => onHandleChange("title", e.target.value)}
          placeholder="Insert todo"
        />

        {loading ? (
          <p style={{ fontSize: 13, fontWeight: "600", color: "green" }}>
            Adding...
          </p>
        ) : (
          <button type="submit">
            <i className="fas fa-plus" />
            <span>Add</span>
          </button>
        )}
      </form>
    </div>
  );
}

Form.propTypes = {
  todos: PropTypes.object.isRequired,
  onHandleChange: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default Form;
