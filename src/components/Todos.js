import React from "react";
import Proptypes from "prop-types";

import TodoItem from "./TodoItem";

function Todos({ todos, onDelete, onToggle }) {
  return (
    <div>
      {todos.length > 0 ? (
        todos.map((todo, index) => (
          <TodoItem
            key={index}
            todo={todo}
            onDelete={onDelete}
            onToggle={onToggle}
          />
        ))
      ) : (
        <p className="text-center">Tidak ada catatan</p>
      )}
    </div>
  );
}

Todos.propTypes = {
  todos: Proptypes.arrayOf(
    Proptypes.shape({
      id: Proptypes.number,
      title: Proptypes.string,
      completed: Proptypes.bool,
    })
  ),
  onDelete: Proptypes.func.isRequired,
  onToggle: Proptypes.func.isRequired,
};

export default Todos;
