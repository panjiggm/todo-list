import React, { useState, useEffect } from "react";
import Axios from "axios";

import Form from "./components/Form";
import Todos from "./components/Todos";

function App() {
  const [todos, setTodos] = useState([]);
  const [formTodo, setFormTodo] = useState({ title: "", completed: false });
  const [loading, setLoading] = useState({
    get: false,
    post: false,
  });

  // GET Todos from Public API
  useEffect(() => {
    const getTodos = async () => {
      try {
        setLoading({ get: true });
        const response = await Axios.get(
          "https://jsonplaceholder.typicode.com/todos?_limit=5"
        );
        const data = await response.data;

        const result = data.reverse();

        setTodos(result);
        setLoading({ get: false });
      } catch (error) {
        console.log(error);
      }
    };

    getTodos();
  }, []);

  //
  const handleTodoChange = (name, value) => {
    setFormTodo({ ...formTodo, [name]: value });
  };

  //
  const handleCreate = async () => {
    const { title, completed } = formTodo;

    if (title === "") {
      alert("Please insert todos");
    } else {
      try {
        setLoading({ post: true });
        const response = await Axios.post(
          "https://jsonplaceholder.typicode.com/todos",
          {
            title,
            completed,
          }
        );

        const result = await response.data;

        setTodos([result, ...todos]);
        setLoading({ post: false });
        setFormTodo({ title: "" });
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleDelete = async (id) => {
    try {
      await Axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);
      await setTodos([...todos.filter((todo) => todo.id !== id)]);
    } catch (error) {
      console.log(error);
    }
  };

  const handleToggle = (id) => {
    setTodos([
      ...todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }

        return todo;
      }),
    ]);
  };

  return (
    <div className="container">
      <h2>Todo-List</h2>
      <div>
        <Form
          todos={formTodo}
          onHandleChange={handleTodoChange}
          onCreate={handleCreate}
          loading={loading.post}
        />
      </div>
      <div>
        {loading.get ? (
          <div className="text-center">Loading...</div>
        ) : (
          <Todos
            todos={todos}
            onDelete={handleDelete}
            onToggle={handleToggle}
          />
        )}
      </div>
    </div>
  );
}

export default App;
