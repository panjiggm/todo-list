<h1 align="center">
  Simple Todo List
</h1>

## A deadly simple todo-list with [React](https://reactjs.org/) using [JSONPlacehoder](https://jsonplaceholder.typicode.com/) Public API.

## 🌟 Features

- ✔ Add Todo
- ✔ Completed Todo
- ✔ Delete Todo

## 🚀 Installation

```sh
# clone from gitlab
git clone git@gitlab.com:panjiggm/todo-list.git
or
git clone https://gitlab.com/panjiggm/todo-list.git

# go to project folder
cd todo-list
# install dependecy
yarn or npm install
# run the aap
yarn start or npm start
```
